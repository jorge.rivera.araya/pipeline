import React from 'react'
import {Image} from './styles'

const DEFAULT_IMAGE = "https://i5.walmartimages.ca/images/Enlarge/094/514/6000200094514.jpg"

export const Category = ({cover = DEFAULT_IMAGE,path="#"})=>(
    <div>
        <div>PIPELINE</div>
        <a href={path}>
            <Image src={cover}/>
        </a>
    </div>
)